
first<-ts(LakeHuron[1:(length(LakeHuron)/2)], start=1875,
end=(1875+length(LakeHuron)/2))
second<-ts(LakeHuron[(length(LakeHuron)/2+1):length(LakeHuron)],
start=(1875+length(LakeHuron)/2+1),
end=1875+length(LakeHuron))

par(mfrow=c(1,2))

plot(first)
abline(h=mean(first), col="red")
abline(h=median(first), col="blue")
abline(h=mean(first)+sd(first), col="green", lty="dashed")
abline(h=mean(first)-sd(first), col="green", lty="dashed")

plot(second)
abline(h=mean(second), col="red")
abline(h=median(second), col="blue")
abline(h=mean(second)+sd(second), col="green", lty="dashed")
abline(h=mean(second)-sd(second), col="green", lty="dashed")

plot(first, ylim=c(min(LakeHuron), max(LakeHuron)))
abline(h=mean(first), col="red")
abline(h=median(first), col="blue")
abline(h=mean(first)+sd(first), col="green", lty="dashed")
abline(h=mean(first)-sd(first), col="green", lty="dashed")

plot(second, ylim=c(min(LakeHuron), max(LakeHuron)))
abline(h=mean(second), col="red")
abline(h=median(second), col="blue")
abline(h=mean(second)+sd(second), col="green", lty="dashed")
abline(h=mean(second)-sd(second), col="green", lty="dashed")

hist(LakeHuron)

hist(LakeHuron, breaks=1, main="Two Bins")
hist(LakeHuron, breaks=500, main="501 Bins")

?USArrests

boxplot(USArrests)

par(mfrow=c(1,4))
boxplot(USArrests$Murder, main="Murder")
boxplot(USArrests$Assault, main="Assault")
boxplot(USArrests$UrbanPop, main="UrbanPop")
boxplot(USArrests$Rape, main="Rape")

head(iris)
par(mfrow=c(1,1))
plot(iris$Petal.Length~iris$Petal.Width)

plot(iris$Petal.Length~iris$Petal.Width)
lm1<-lm(iris$Petal.Length~iris$Petal.Width)
abline(lm1)

plot(iris$Petal.Length~iris$Petal.Width, col=iris$Species)
lmsetosa<-lm(Petal.Length~Petal.Width,
data=iris[iris$Species=="setosa",])
lmversicolor<-lm(Petal.Length~Petal.Width,
data=iris[iris$Species=="versicolor",])
lmvirginica<-lm(Petal.Length~Petal.Width,
data=iris[iris$Species=="virginica",])
abline(lmsetosa, col='black')
abline(lmversicolor, col='red')
abline(lmvirginica, col='green')
